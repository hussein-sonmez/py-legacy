import mysql.connector
import config


def contextify():
	context = mysql.connector.connect(**config.dbconfig)
	context.close()
