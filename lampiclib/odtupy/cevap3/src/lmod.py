def l(base, numdigit):
  import math;
  N = int(math.pow(base, numdigit));
  #N = mod^n
  M = numdigit;
  #N is row count
  #M is column count
  word = "";
  #word is one unit row of binary permutation
  for i in range(N):
    word = "";
    for j in range(M):
      word += str(int((i / math.pow(base, M-j-1))) % base);
    print(word);

#l(2, 2);

def magicpow(base, k):
  #return base^k
  import math;
  w = "";
  #M = k + 1: base^k has k+1 digits
  M = k + 1;
  for j in range(M):
    #w += str(int(math.pow(base, k - (M-j-1))) % base);
    w += str(int(math.pow(base, j)) % base);
  print(w);

#magicpow(3, 100);

def exp2decimal(n):
  '''
    returns 2^n string representation
    b = 2^n / 2^(M-j-1)
    b = 10^(n*log10(2)-(M-j-1))
  '''
  import math;
  def balance(f):
    return math.ceil(f)%2==0 \
           and math.ceil(f) \
           or math.floor(f);
  def pw(base, exp):
    return int(math.pow(base, exp));
  def pwbalance(base, exp):
    return balance(math.pow(base, exp));
  log2 = math.log10(2);

  M = int(n*log2) + 1;
  b = ""; w = "";
  for j in range(M):
    try:
      b = str(pw(10, n*log2 - (M-j-1)) % 10);
      print(pwbalance(10, n*log2 - (M-j-1)), end = "||");
    except Exception as ex:
      return ex;
    w += b;
  return w;

#exp2decimal(7);

def exp2decimalp(n, k):
  '''
  returns 2^n + k;
  '''
  def dignum(num):
    r = 1;
    while(num > 9):
      r += 1;
      num /= 10;
    return r;

  exp2 = exp2decimal(n);
  lenK = dignum(k);
  slc = exp2[-lenK:];
  e = int(slc);

  #add k to slc:
  if (e + k >= e and dignum(e+k)==dignum(e)):
    e += k;
    exp2 = exp2[:len(exp2)-lenK] + str(e);
  else:
    return "carry!";
  
    

  #turn result:
  return exp2;


#exp2decimalp(100, 99); #2^5 + 5 = 37

##for n in range(10000):
##  print("exp2decimal({0}) : {1}\nexp2decimalp({0}, {2}) : {3}\n"\
##        .format(n, exp2decimal(n), n-1, exp2decimalp(n, n-1)),);

n = int(input("n:\n"));
print("exp2decimal({0}) : {1}\n".format(n, exp2decimal(n)));
print("Must be equal to 1 << {0} : {1}".format(n, 1 << n));
