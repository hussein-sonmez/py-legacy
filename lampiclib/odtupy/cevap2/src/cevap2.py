
import sys;
text = open(sys.argv[1]).read();

import re;
words = re.findall(r'[\s\d]*([a-zA-Z]+)[\s\d]*', text);

results = [];

for i in range(len(words)):
  for j in range(i + 1, len(words)):
    if words[i] == words[j]:
      for r in results:
        if words[i] == r:
          results.remove(r)
      results.append(words[i]);

print(",".join(results));




