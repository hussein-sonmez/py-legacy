class Multiray(object):
  def __init__(self, *k):
    self.Dimensions = k;
    self.Length = 1;
    self.Position = -1;
    self._InitLength();
    self._InitArray();

  def _InitLength(self):
    for i in self.Dimensions:
      self.Length *= i;

  def _InitArray(self):
    self._Arr2D = [None]*self.Length;

  def __setitem__(self, keyTuple, item):
    keys = self._CheckKeys(keyTuple);
    self.Position = self._EvaluateIndex(keys, k=1);
    self._Arr2D[self.Position] = item
    
  def __getitem__(self, keyTuple):
    keys = self._CheckKeys(keyTuple);
    self.Position = self._EvaluateIndex(keys, k=1);
    return self._Arr2D[self.Position];

  def _CheckKeys(self, keyTuple):
    assert(len(keyTuple) == len(self.Dimensions));
    keyList = list(keyTuple);
    limit = len(keyList);
    for i in range(limit):
      assert(keyList[i] < self.Dimensions[i] \
             and keyList[i] >= -self.Dimensions[i]);
      #for minus indexes:
      #like a[1,-2]
      if keyList[i] < 0:
        keyList[i] += self.Dimensions[i];
    return keyList;

  def _EvaluateIndex(self, keyTuple, k):
    dLen = len(self.Dimensions);
    if k == dLen: return keyTuple[-1];
    t = 1;
    for i in range(k, dLen):
      t *= self.Dimensions[i]
    t *= keyTuple[k - 1];
    return t + self._EvaluateIndex(keyTuple, k=k+1);

  def GetLength(self, d):
    assert(isinstance(d, int));
    assert(d >= 0 and d < len(self.Dimensions));
    return self.Dimensions[d];
    
if __name__ == "__main__":
  a = Multiray(2, 5, 20, 6);
  a[1,4,19,5] = "got me!";
  print(a[1,4,19,-1]);

  for i in range(a.GetLength(0)):
    for j in range(a.GetLength(1)):
      for k in range(a.GetLength(2)):
        for l in range(a.GetLength(3)):
          print("a[{1:04}]={0}".format(a[i, j, k, l], a.Position));
    
