'''
Created on 12 Şub 2011

@author: dr.ozgur.sonmez
'''

from datetime import datetime
from funcet.MFuncet import TFuncet

class TCrawler(object):
  def __init__(self, denomEvalMoncet = None):
    self.StartTime = None;
    self.EndTime = None;
    self.DenomEvalMoncet = TFuncet();
    if denomEvalMoncet is not None:
      self.DenomEvalFuncet.Append(denomEvalMoncet);
  
  def Start(self):
    if self.DenomEvalFuncet is None:
      raise Exception("Please set self.DenomEvalFuncet first.");
    self.Denominator = self.DenomEvalMoncet.Invoke()[0];
    self.StartTime = datetime.now();  
  
  def ReportProgress(self):
