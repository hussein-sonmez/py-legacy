'''
Created on 31 Oca 2011

@author: dr.ozgur.sonmez
'''

from datetime import datetime

class TWalker(object):
  def __init__(self):
    self.start = None;
    self.now = datetime.now;
    
  def Start(self):
    self.start = self.now();
    
  def Stop(self):
    offset = self.start
    self.start = None
    return self.now() - offset
    
  def Lapse(self):
    offset = self.start
    self.start = self.now()
    return self.now() - offset
    
  def StopAndReport(self):
    print("Operation completed in {0}".format(self.Stop()));
    
  def LapseAndReport(self):
    print("Operation lapsed in {0}".format(self.Lapse()));
