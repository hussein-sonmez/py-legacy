# To change this template, choose Tools | Templates
# and open the template in the editor.

__author__ = "Ozgur"
__date__ = "$23.Eyl.2010 20:47:07$"

import os
import os.path as ph
walk = os.walk

pathjoin = ph.join
pathsplit = ph.split
fileext = ph.splitext

def GetChildDirs(path):
    if ph.isdir(path):
        dirs = []
        [dirs.extend([ph.join(r, d) for d in ds]) for r, ds, fs in walk(path)]
        return dirs
    else:
        raise ValueError('path: {0} is not a dir'.format(path))
    
def GetChildFiles(path):
    if ph.isdir(path):
        files = []
        [files.extend([ph.join(r, f) for f in fs]) for r, ds, fs in walk(path)]
        return files
    else:
        raise ValueError('path: {0} is not a dir'.format(path))

def IsPackage(path):
    return any([pathsplit(f)[-1] == '__init__.py' or 'setup.py'\
                for f in GetChildFiles(path)])

def GetPyFilesExceptInit(parentPackage):
    return [f for f in GetChildFiles(parentPackage) \
        if pathsplit(f)[-1] != '__init__.py' and fileext(f)[1] == '.py']

if __name__ == "__main__":
    print ("Hello World")
