from . import dirtools
# To change this template, choose Tools | Templates
# and open the template in the editor.

__author__ = "Ozgur"
__date__ = "$23.Eyl.2010 21:26:09$"


def GetModules(parentPackage):
    return [dirtools.pathsplit(f)[-1].replace('.py', '') for f in dirtools.GetPyFilesExceptInit(parentPackage)]

def InsertInits(rootDir):
    InsertInitsRecursive(rootDir)

def InsertInitsRecursive(parentDir):
    childDirs = dirtools.GetChildDirs(parentDir)
    childModules = GetModules(parentDir)

    #set entry:
    if len(childModules) != 0:
        #there are modules here
        initPath = dirtools.pathjoin(parentDir, '__init__.py')
        fInit = open(initPath, 'w');
        all = '__all__ = [' + ', '.join(['"' + cm + '"' for cm in childModules]) + ']\n';
#        imports = '\n'.join(['import '+cm for cm in childModules])
        fInit.write(all);
#        fInit.write(imports);
        fInit.close();
    for cd in childDirs:
        InsertInitsRecursive(cd)




if __name__ == "__main__":
    import os;
    cwd = os.getcwd();
    print("\n".join(["module: %s" % m for m in GetModules(cwd)]));
