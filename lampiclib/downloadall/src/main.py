
from Engine.hextractor import HtmlExtractor;

import tkinter as tk;
from Gui.layout import CustomLayout;

root = tk.Tk();

import sys;

url = len(sys.argv) > 1 and sys.argv[1] or "http://wiki.python.org/moin/";  
he = HtmlExtractor(url);
lbLinks = tk.Listbox(root);
btnFill = tk.Button(root);

def lstHandle():
  for l in he.Links:
    lbLinks.insert(0, l);

def btnHandler():
  pass

lstLayout = CustomLayout(he.Links, lbLinks);
btnLayout = CustomLayout("Fill", btnFill);

lstLayout.BindData = lstHandle;
btnLayout.BindData = btnHandler;
lstLayout.PerformLayout()
input();
