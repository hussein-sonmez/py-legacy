from Gui.data import Data;
from Gui.control import Control;

class CustomLayout(Data, Control):
  def __init__(self, data, ctr):
    Data.__init__(self, data);
    Control.__init__(self, ctr);

  def BindData(self):
    pass;

  def PerformLayout(self):
    self.BindData();
