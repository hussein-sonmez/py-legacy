from mod.MPySpeller import TPySpeller;

def pyspell(text):
  ps = TPySpeller();
  ps.DumpCheck(text);

if __name__ == "__main__":
  import sys, os
  path = os.path.join(os.getcwd(), "text.txt")
  textfile = open(path, "r")
  text = textfile.read()
  textfile.close()
  pyspell(text)
  input()
