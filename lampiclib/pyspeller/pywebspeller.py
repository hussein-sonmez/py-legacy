from mod.MPySpeller import TPySpeller
import urllib.request as ur
import sys
import importer;
notifier = importer.importkit("out", "notifiers")

def pywebspell(rooturl):
  text = ""
  notifier.printf("reading url {0}..", rooturl)
  try:
    text = str(ur.urlopen(rooturl).read())
    notifier.printf("{0} read.", rooturl)
  except:
    notifier.exitf("url {0} could not be opened.", rooturl)    
  ps = TPySpeller()
  ps.DumpCheck(text)

if __name__ == "__main__":
  pywebspell("http://www.w3.org/");
  input("enter to exit..")
