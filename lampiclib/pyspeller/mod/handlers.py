def soundshandler():
  sounds = tuple([chr(i + 97) for i in range(26)])
  vowels = ("a", "e", "i", "o", "u")
  consonants = set(sounds).difference(set(vowels))
  return vowels, consonants

def maxrecurrencehandler():
  return 2, 3
