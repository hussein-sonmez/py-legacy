
if __name__ == "__main__":
  from handlers import *
else:
  from mod.handlers import *

import re
class TPySpeller(object):
  class Handlers:
    def Sounds():
      '''
      returns (vowels, consonants)
      '''
      return soundshandler()
    def MaxRecurrences():
      '''
      returns sound maximum lengths allowed in a word
      first for vowels second for consonants
      can be delegated: TPySpeller.Handlers.MaxRecurrences = lambda..
      '''
      return maxrecurrencehandler()
    
  def __init__(self):
    self.init()

  def init(self):
    try:
      self.vowels, self.consonants = TPySpeller.Handlers.Sounds()
      #vowel recurrence count and consonant recurrence count
      self.vrecur, self.crecur = TPySpeller.Handlers.MaxRecurrences()
      for v in self.vowels:
        if not isinstance(v, str):
          raise ValueError("Vowel:'{0}' must be string.".format(v))
      for c in self.consonants:
        if not isinstance(c, str):
          raise ValueError("Consonant'{0}' must be string.".format(s))
    except Exception as ex:
      import sys
      print("Message", ex)
      sys.exit(0)

  def SpellCheck(self, text):
    words = re.findall("\w+", text)
    #words exceeds or not list
    wordsok = []
    for w in words:
      #recurrence counters
      vrecur, crecur = 0, 0
      #this word exceeds?
      exceeds = False
      for c in w:
        if c in self.vowels:
          vrecur += 1
          crecur = 0
        elif c in self.consonants:
          crecur += 1
          vrecur = 0
        else:
          pass
          #raise ValueError()
        if vrecur > self.vrecur or crecur > self.crecur:
          exceeds = True
          break
      wordsok.append(not exceeds)
    return dict(zip(words, wordsok))

  def DumpCheck(self, text):
    result = self.SpellCheck(text)
    msg = ""
    if not False in result.values():
      msg = "\n".join(["{0:15} : {1}".format(k, v) for k, v in result.items()])\
            + "\nSpell check succeeded"
    else:
      msg = "Spell check failed with: "\
             + ", ".join([k for k, v in result.items() if not v])
    print(msg)

