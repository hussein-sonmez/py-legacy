from input._const import consts
from core._worker import worker
# To change this template, choose Tools | Templates
# and open the template in the editor.

__author__ = "Ozgur"
__date__ = "$23.Eyl.2010 13:01:07$"

class backfiller(worker):
    def __init__(self, tagseq):
        worker.__init__(self, self);
        self.tagseq = tagseq;
        self.alpha = 0;
        self.beta = 0;

    def _w_0_fill(self):
        end_i = len(self.tagseq) - 1;
        for i in range(end_i, -1, -1):
            t = self.tagseq[i];
            if t.Type == consts["TagTypes"]["Start"]:
                self.alpha = self.alpha + 1;
            elif t.Type == consts["TagTypes"]["End"]:
                self.beta = self.beta + 1;
            else:
                continue
            t.alpha = self.alpha;
            t.beta = self.beta;
#            dumper.dump('t.Index : %s , t.Type : %s , t.alpha : %s , t.beta : %s , t.x : %s , t.y : %s ', \
#                t.Index, t.Type, t.alpha, t.beta, t.x, t.y)

if __name__ == "__main__":
    print ("Hello World")
