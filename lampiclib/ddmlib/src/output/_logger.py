'''
Created on 22 Eyl 2010

@author: Ozgur
'''

from input._const import consts;
import os, datetime;

class _logger(object):
    def __init__(self, logfile=None):
        logpath = os.path.join(os.path.curdir, consts["Logger"]["LogFile"]);
        if logfile == None:
            self.logfile = open(logpath, 'a+');
        else:
            self.logfile = logfile;
            
    def logn(self, annotation, entry):
        e = '%s logged as "%s" at time %s' % (annotation, \
                                             str(entry), \
                                             str(datetime.date.today())) + \
                                             consts["Logger"]["NewLine"];
        self.logfile.write(e);
        
    def log(self, annotation, entry):
        e = '%s logged as "%s" at time %s' % (annotation, \
                                             str(entry), \
                                             str(datetime.date.today())) + \
                                             consts["Logger"]["Seperator"];
        self.logfile.write(e);

#initialize:
logger = _logger();

if __name__ == "__main__":
    for x in range(1000):
        logger.log('Here is the magic numbers:', x);
