'''
Created on 21 Eyl 2010

@author: Ozgur
'''
from input._const import consts
from process import toolbox

class worker(object):
    def __init__(self, obj, mList=None):
        if mList == None:
            mList = [method for method in dir(obj) if hasattr(getattr(obj, method), "__call__")];
            mList = filter(self.__check_for_method, mList);
            mList = sorted(mList)
        self.obj = obj;
        self.methodList = mList;
        self.omits = None

    def __check_for_method(self, m):
        '''Checks if Method Name Carries "_w_" at Front''' 
        return m[0:3] == consts["WorkerPrefix"] and True or False

    def __filter_by_omits(self, m):
        for o in self.omits:
            if m[:len(o)] == o:
                return False
        return True
    
    def run(self):
        ml = self.methodList[:];
        for m in self.methodList:
            self.omits = getattr(self.obj, m).__call__();
            ml.remove(m);
            if self.omits == None:
                continue
            if toolbox.is_iterable(self.omits):
                remainder_list = filter(self.__filter_by_omits, ml)
                for remainder in remainder_list:
                    getattr(self.obj, remainder).__call__();
                break;
            
if __name__ == "__main__":
    class sample():
        def m1(self):
            print ('m1 worked');
        def m2(self):
            print ('m2 worked');
        def m3(self):
            print ('m3 worked');
        def m4(self):
            print ('m4 worked');
        def m5(self):
            print ('m5 worked');
    class sample2():
        def _w_0_mvfd(self):
            print ('_w_0_mvfd worked')
            return ["_w_1_", "_w_5_"];
        def _w_1_mvfvdd(self):
            print ('_w_1_mvfvdd worked');
        def _w_2_mggt(self):
            print ('_w_2_mggt worked');
        def _w_3_mert(self):
            print ('_w_3_mert worked');
            return False;
        def _w_4_mvgh(self):
            print ('_w_4_mvgh worked');
    s = sample();
    s2 = sample2();    
#    w = worker(s, ["m1", "m2", "m3", "m4", "m5"]);
    w = worker(s2);
    w.run();
