from process import toolbox
from process._digester import digester
from input._const import consts, TagIdentifier
from output import dumper
# To change this template, choose Tools | Templates
# and open the template in the editor.

__author__ = "Ozgur"
__date__ = "$23.Eyl.2010 16:45:45$"

class ddmreader(object):
    def __init__(self, docpath):
        self.digester = digester(docpath);

    def read(self):
        self.digester.digest()
        isWellBuilt = self.check_integrity()
        if isWellBuilt:
            print ("success")
        else:
            assert False, "not success\n".join([("-"*80 + "\n") * 2] * 2)
        

    def get_text(self):
        return self.digester.doctext

    def check_integrity(self, index=0):
        '''
        checks tag or tag sequence if well built or not
        '''
        seq = self.digester.tagseq;
        if (index == 0):
            if (len([t for t in seq if toolbox.is_start(t)]) != \
                len([t for t in seq if toolbox.is_end(t)])):
                print ('check_integrity error: start-end count mismatch\nstart count: %d end count: %d' \
                    % (len([t for t in seq if toolbox.is_start(t)]), len([t for t in seq if toolbox.is_end(t)])))
                return False
        tag = seq[index]
        isOK = None
        if tag.Type == consts["TagTypes"]["Start"]:
            isOK = tag.x - tag.y == tag.beta - tag.alpha + 1;
        elif tag.Type == consts["TagTypes"]["End"]:
            isOK = tag.x - tag.y == tag.beta - tag.alpha - 1;
        else:
            isOK = self.check_integrity(index + 1);
        if not isOK:
            dumper.dumpn('Check Integrity Failed:')
            #dumper.dumpn('tag.x, tag.y, tag.alpha, tag.beta', tag.x, tag.y, tag.alpha, tag.beta)
        assert(isOK != None)
        return isOK;

    def get_start(self, index):
        end = self.digester.tagseq[index];
        if not toolbox.is_border(end):
            return end
        limit = len(self.digester.tagseq);
        assert(index < limit);
        end_depth = end.Depth
        start = None;
        for i in range(end.Index, -1, -1):
            start = self.digester.tagseq[i];
            if start.Depth == end_depth:
                if start.Type == consts["TagTypes"]["Start"]:
                    break
        return start;

    def get_end(self, index):
        start = self.digester.tagseq[index];
        if not toolbox.is_border(start):
            return start
        limit = len(self.digester.tagseq);
        assert(index < limit);
        start_depth = start.Depth
        end = None;
        for i in range(index, limit, 1):
            end = self.digester.tagseq[i];
            if end.Depth == start_depth:
                if end.Type == consts["TagTypes"]["End"]:
                    break
        return end;

    def __get_elemstr_by_index(self, index):
        start = self.digester.tagseq[index].iBegin;
        if not toolbox.is_border(self.digester.tagseq[index]):
            end = self.digester.tagseq[index].iEnd + 1
        else:
            end = self.get_end(index).iEnd + 1;
        corpus = self.digester.doctext[start:end];
        return corpus;

    def GetElementString(self, starttagname):
        for t in self.digester.tagseq:
            if t.Name == starttagname and toolbox.is_start(t):
                return self.__get_elemstr_by_index(t.Index)
        return None
    
    def GetElementStringById(self, starttagname, id):
        for t in self.digester.tagseq:
            if t.Name == starttagname and toolbox.is_start(t):
                for n, v in t.Attributes.items():
                    if n == "id" and v == id:
                        return self.__get_elemstr_by_index(t.Index)
    
    def CollectElementsByClass(self, clsvalue):
        collected = []
        for t in self.digester.tagseq:
            for n, v in t.Attributes.items():
                if n == "class" and v == clsvalue:
                    collected.append({"Start" : t, "End" : self.get_end(t.Index)})
        return collected
        
    def GetElementSentence(self, index):
        tag = self.digester.tagseq[index];
        content = None;
        attrstr = toolbox.extract_attr(tag);
        if len(attrstr) != 0:
            attrstr = " " + attrstr;
        if tag.Type == consts["TagTypes"]["Start"]:
            content = '%s%s%s%s' % (TagIdentifier.Start, \
                tag.Name, attrstr, \
                TagIdentifier.End);
        elif tag.Type == consts["TagTypes"]["End"]:
            content = '%s%s%s%s%s' % (TagIdentifier.Start, \
                TagIdentifier.Finalizer, \
                tag.Name, attrstr, \
                TagIdentifier.End);
        elif tag.Type == consts["TagTypes"]["Single"]:
            content = '%s%s%s%s%s' % (TagIdentifier.Start, \
                tag.Name, \
                TagIdentifier.Finalizer, \
                TagIdentifier.End);
        return content;


    def collect_by_attrval(self, val):
        pass


    def collect_by_attrname(self, name):
        pass


if __name__ == "__main__":
    print ("Hello World")
