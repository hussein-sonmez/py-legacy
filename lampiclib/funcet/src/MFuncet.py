'''
Created on 12 Sub 2011

@author: dr.ozgur.sonmez
'''

class TFuncet(object):
  """
  FUNCtionBuckET:
  Function Holder and Invoker Class.
  To Enumerate Functions Use YieldInvoke
  """
  def __init__(self):
    self.__funcet = [];
    
  def Append(self, moncet, *args):
    self.__funcet.append((moncet, args));
    
  def Invoke(self):
    results = [];
    for ma in self.__funcet:
      m = ma[0];
      a = ma[1];
      if len(a) == 0:
        results.append(m());
      else:
        results.append(m(*a));
    return results;
  
  def Yield(self):
    for fn in self.__funcet:
      yield fn
  
  def Reset(self):
    del(self.__funcet);
    self.__funcet = [];
  
if __name__ == "__main__":
  fn = TFuncet();
  def x(i) : return i
  for i in range(10):
    fn.Append(x, i);
  print(fn.Invoke())
  
