# -*- coding: cp1254 -*-
import Tkinter
import tkSnack

root = Tkinter.Tk()

tkSnack.initializeSnack(root)
snd = tkSnack.Sound()
devices = tkSnack.audio.inputDevices()
tkSnack.audio.selectInput(devices[1])  #bu ayg�t bende mikrofon ayg�t�
#sendeki ayg�tlar� "print devices" komutla g�rebilirsin.

def stop():
    snd.stop()

def start():
    snd.record()
    
def play():
    snd.play()
    print (snd.max()) #iste senin kullanaca��n metod.
    #bu metodun start=.. ve end=.. optionlar� var. Bu se�eneklere de�er atayarak
    #bir ses par�as�n�n en y�ksek �rnekleme h�zlar�na ula�abilirsin
    #ki bu da genlikler oluyor galiba(amplitude)
    print (snd.min()) #bu da en d���k �rnekleme h�z� i�in verilen metod.
    #bu metodun da start ve end se�enekleri var.
    #mesela snd.min(start=0, end=100) sana ilk 100 birimlik ses sinyalinin
    #en d���k genli�ini verecektir. Bunu sinyal kuvvetini hesaplamak i�in kulla-
    #nabilirsin.


print (devices)

f = Tkinter.Frame()
f.pack()
Tkinter.Button(f, bitmap='snackRecord', fg='red', command=start).pack(side='left')
Tkinter.Button(f, bitmap='snackStop', command=stop).pack(side='left')
Tkinter.Button(f, text='Exit', command=root.quit).pack(side='left')
Tkinter.Button(f, bitmap='snackPlay', command=play).pack(side='left')
root.mainloop()
