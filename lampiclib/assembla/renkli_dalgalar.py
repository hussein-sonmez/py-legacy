from pygame import *
import random

class AnaCerceve():
    def __init__(self, cozunurluk):
        #self.resim = image.load(resimyolu)
        self.cozunurluk = cozunurluk
        display.init()
        self.ekran = display.set_mode(self.cozunurluk)
        self.ekran.fill((0,0,255))
        self.tual = Surface(self.cozunurluk, SRCALPHA, 32)
        self.tual.fill((255,0,0,255))
        self.ekran.blit(self.tual, (0,0))
    
    def dalgalan(self, dalgaboyu):
        genislik = self.cozunurluk[0]
        yukseklik = self.cozunurluk[1]
        ortayauzk = int(min(genislik, yukseklik)/2)
        self.yaricaplar = range(dalgaboyu,ortayauzk)
        karesys = len(self.yaricaplar)
        self.merkezler = (
            (int(mx + genislik/2), int(my + yukseklik/2)) \
            for mx,my in zip(                             \
                random.sample(range(-ortayauzk,ortayauzk), karesys),   \
                random.sample(range(-ortayauzk,ortayauzk), karesys)    \
            )                                               \
        )
        self.renkler =                   \
            (                       \
                (r,g,b,a) for (r,g,b,a) \
                in zip(random.sample(range(256),karesys), \
                       random.sample(range(256),karesys), \
                       random.sample(range(256),karesys),
                       range(0,256)) \
            )
        for m in self.merkezler:
            for r,y in zip(self.renkler, self.yaricaplar):
                display.update(draw.circle(self.tual,r,m,y,dalgaboyu))
                self.ekran.blit(self.tual, (0,0))
                display.flip()
        

if __name__ == "__main__":
    ekran = AnaCerceve((200,200))
    event.pump()
    isContinue = True
    while isContinue:
        print ("continuing..")
        ekran.dalgalan(2)
        for e in event.get():
            if e.type == QUIT:
                print ("game over")
                display.quit()
                isContinue = False
                break
