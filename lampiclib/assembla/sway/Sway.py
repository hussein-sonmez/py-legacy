from pygame import *
from math import *
import random
import numpy

import color
from progressbar import *

class AnaCerceve:
    def __init__(self, resimyolu):
        self.resim = image.load(resimyolu)
        #self.cozunurluk = (self.resim.get_width(), self.resim.get_height())
        self.sizes = {}
        self.pads = (5,5)
        self.sizes["screen"] = (self.resim.get_width()+2*self.pads[0], \
                               self.resim.get_height()+2*self.pads[1])
        self.sizes["layer1"] = self.resim.get_size()
        display.init()
        surfarray.use_arraytype("numpy")
        self.screen = display.set_mode(self.sizes["screen"])
        #self.layer1 = pygame.Surface(self.sizes['layer1'], flags=SRCALPHA).convert_alpha()
        self.layer1 = pygame.Surface(self.sizes["layer1"]).convert()

        #self.layer1.fill((255,0,255,255))      #purple full transparent
        self.layer1.blit(self.resim, (0,0))
        display.update(self.screen.blit(self.layer1, self.pads)) 
        self.layer1arr3d = surfarray.array3d(self.layer1)
        
        
        
    def get_ring(self, center, radius):
        '''draws an imaginary circle around center with radius.
           returns list of x,y coordinates of this circle.
           the radius must be an integer.
           center is a two members tuple: (x,y)
        '''
        cx, cy = center
        coords = []
        r2 = radius * radius
        coords.append([cx-1, cy+radius-1])
        coords.append([cx-1, cy-radius-1])
        coords.append([cx+radius-1, cy-1])
        coords.append([cx-radius-1, cy-1])

        y = radius;
        x = 1;
        y = int(sqrt(r2-1) + 0.5);
        while x < y:
            coords.append([cx+x-1, cy+y-1])
            coords.append([cx+x-1, cy-y-1])
            coords.append([cx-x-1, cy+y-1])
            coords.append([cx-x-1, cy-y-1])
            coords.append([cx+y-1, cy+x-1])
            coords.append([cx+y-1, cy-x-1])
            coords.append([cx-y-1, cy+x-1])
            coords.append([cx-y-1, cy-x-1])
            x += 1
            y = int(sqrt(r2-x*x) + 0.5);
        coords.append([cx+x-1, cy+y-1])
        coords.append([cx+x-1, cy-y-1])
        coords.append([cx-x-1, cy+y-1])
        coords.append([cx-x-1, cy-y-1])
        return [(0,0) if (x<0 or y<0) else (x,y) for (x,y) in coords]

    def dalgalan(self, center, dalgaboyu, wavenum, density):
        if density>1.0 or density<0.0:
            print ("Incorrect Usage!! density can not be greater than 1.0 or less then 0.")
            sys.exit(1)
        genislik    = self.sizes["layer1"][0]
        yukseklik   = self.sizes["layer1"][1]
        minr = min(genislik, yukseklik)/2
#        karesys = len(self.yaricaplar)
#        self.merkezler = (
#            (mx+genislik/2, my+yukseklik/2) \
#            for mx,my in zip(                             \
#                random.sample(range(-ortayauzk,ortayauzk), karesys),   \
#                random.sample(range(-ortayauzk,ortayauzk), karesys)    \
#            )                                               \
#        )
        newcolor = None
        r = 1
        qtnt = 0.0
        add = 1.0/dalgaboyu
        qtntl = []
        while qtnt<density:
            qtntl.append(qtnt)
            qtnt += add
        while qtnt>-density:
            qtntl.append(qtnt)
            qtnt -= add
        while qtnt<0.0:
            qtntl.append(qtnt)
            qtnt += add       
        progbarArgs = ProgBarArgs(self.layer1, (150,25))
        progbarArgs.whole   = wavenum
        progbarArgs.step    = 1
        running = ProgressBar(progbarArgs)
        for wave in range(wavenum):
            for q in qtntl:
                ring_points = self.get_ring(center, r)
                for x,y in ring_points:
                    if x>genislik-1 or y>yukseklik-1:
                        continue
                    newcolor = color.alter_brightness(self.layer1arr3d[x,y], q)
                    self.layer1.set_at((x,y), newcolor)
                r += 1
                display.update(self.screen.blit(self.layer1, self.pads))
            running.makestep()
        running.reset()

    def reset(self):
        self.layer1.blit(self.resim, (0,0))
        display.update(self.screen.blit(self.layer1, self.pads))
        
if __name__ == "__main__":
    dalgalar = AnaCerceve("assets\\sway.jpg")
    isContinue = True
    while isContinue:
        for e in event.get():
            if e.type == QUIT:
                print ("game over")
                display.quit()
                isContinue = False
                break
            elif e.type == MOUSEBUTTONDOWN:
                if e.button == 1:
                    dalgalar.dalgalan(e.pos,30,5,0.3)
                    print ("wave constructed")
                    break
                elif e.button == 3:
                    dalgalar.reset()
