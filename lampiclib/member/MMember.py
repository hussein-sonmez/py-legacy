
from MNotifier import *

# assertf = ntfy.assertf
# exitf = ntfy.exitf
# printf = ntfy.printf
# asserts = ntfy.asserts
# dump = ntfy.dump

class Sign:
  pass
Sign.Positive = True
Sign.Negative = not Sign.Positive

class Append:
  pass
Append.ToLowerMost = 1
Append.ToUpperMost = 2

class TMember(object):      
  def __init__(self, base):
    asserts(base != 0, "InvalidValue", base, 10, locals())
    self.__Base = base
    self.__Sign = Sign.Positive
    self.__Digits = []
    
  @staticmethod
  def PowerOfBase(p, base):
    result = TMember.FromInt(1, base)
    for i in range(p):
      result *= base
    return result
  
  @staticmethod
  def FromList(memlist, base):
    assertf(base == 10, "only octal base 10 accepted")
    result = TMember(base)
    for i in range(len(memlist)):
        result.AppendDigit(int(memlist[i]))
    return result

  @staticmethod
  def FromInt(memint, base):
    assertf(base == 10, "only octal base 10 accepted")
    asserts(isinstance(memint, int)\
            , "TypesMismatch"\
            , memint, int)
    assertf(memint >= 0);
    result = TMember(base)
    c, r = 0, memint
    while r >= base:
      c, r = SCalcBase.Split(r, base)
      result.AppendDigit(c)
    result.AppendDigit(r)
    return result

  @staticmethod 
  def FromMember(mem):
    assertf(isinstance(mem, TMember)\
            , "memint must be type(<{0}>) but is type(<{1}>)"\
            , type(TMember), type(mem))
    result = TMember(mem.Base())
    for i in range(mem.Length()):
      result.AppendDigit(mem[i])
    return result

  @staticmethod
  def FromString(memstr, base):
    octal = TMember(base=10)
    for i in range(len(memstr)):
        octal.AppendDigit(int(memstr[-1-i]))
    if base == 10:
      return octal
    result = TMember(base=base)
    for i in range(octal.Length()):
      result += TMember.PowerOfBase(i, 10)*octal[i]
    return result

  @staticmethod
  def FromAny(anymem, base):
    if isinstance(anymem, TMember):
      return TMember.FromMember(anymem)
    elif isinstance(anymem, str):
      return TMember.FromString(anymem, base)
    elif isinstance(anymem, int):
      return TMember.FromInt(anymem, base)
    else:
      assertf("unidentified instance type<{0}>", type(anymem))

  def __getitem__(self, k):
    assertf(k < self.Length() and k >= -self.Length());
    return self.__Digits[k];

  def __setitem__(self, k, item):
    assertf(isinstance(k, int) and isinstance(item, int));
    assertf(k < self.Length() and k >= -self.Length());
    self.__Digits[k] = item;
  
  def __str__(self):
    if self.Length() == 0:
      s = "0"
    else:
      s = "".join((str(d) for d in self.__Digits[::-1]));
    return s;
  
  def __eq__(self, other):
    return SComparisonCalc.Eq(self, other, self.Base())

  def __gt__(self, other):
    return SComparisonCalc.Gt(self, other, self.Base())
  
  def __ge__(self, other):
    return SComparisonCalc.Ge(self, other, self.Base())
  
  def __lt__(self, other):
    return SComparisonCalc.Lt(self, other, self.Base())

  def __le__(self, other):
    return SComparisonCalc.Le(self, other, self.Base())

  def __add__(self, adder):
    return SAdditionCalc.Add(self, adder, self.Base())

  def __sub__(self, sub):
    return SSubtractionCalc.Absolute(self, sub, self.Base())

  def __mul__(self, multiplicand):
    return SMultiplicationCalc.Multiply(self, multiplicand, self.Base())
  
  def __floordiv__(self, divisor):
    return SDivisionCalc.FloorDivide(self, divisor, self.Base())[1]

  def __mod__(self, mod):
    return SDivisionCalc.Modulus(self, mod)

  def ToDigit(self):
    assertf(self.Length() < 2, "self.Length()<{0}> must be < 2", self.Length())
    return 0 if self.Length() == 0 else self.__Digits[0]

  def Extend(self, m):
    assertf(isinstance(m, TMember));
    for i in range(m.Length()):
      self.AppendDigit(m[i])

  def AppendDigit(self, digit, where=Append.ToUpperMost):
    asserts(isinstance(digit, int), "TypeMismatch", digit, int)
    assertf(digit >= 0 and digit < self.__Base, "digit<{0}> is not in [0,{1})"\
            , digit, self.__Base)
    if where == Append.ToUpperMost:
      self.__Digits.append(digit)
    elif where == Append.ToLowerMost:
      self.__Digits.insert(0, digit)
    return self
  
  def Length(self):
    return len(self.__Digits)

  def Base(self):
    return self.__Base
  
  def ShiftLeft(self, n):
    assertf(isinstance(n, int))
    assertf(n >= 0)
    if n==0:
      return self
    mbegin = TMember(self.__Base)
    for i in range(n):
      mbegin.AppendDigit(0)
    mbegin.Extend(self)
    return mbegin

  def TrimEnd(self):
    if self.Length() == 1:
      if self[0] == 0:
        return self
    for d in self.__Digits[::-1]:
      #if d is not zero; end is reached
      if d != 0: return self
      else:
        #delete last zero
        del(self.__Digits[-1])
    return self

  def Slice(self, start, end, step=1):
    asserts(isinstance(start, int), "TypesMismatch"\
            , start, int)
    asserts(isinstance(end, int), "TypesMismatch"\
            , end, int)
    if start < 0:
      start += self.Length()
      step = -1
    if end < 0:
      end += self.Length()
      step = -1
    assertf(start >= 0 and end < self.Length() and end >=-1\
            , "invalid slice parameters start<{0}>, end<{1}>, Length<{2}>"\
            , start, end, self.Length())
    sliced = TMember(base=self.__Base)
    where = Append.ToUpperMost if step > 0 else Append.ToLowerMost
    for i in range(start, end, step):
      sliced.AppendDigit(self[i], where)
    return sliced
  
####################################
####################################
class SCalcBase:
  @staticmethod
  def Split(dividend, divisor):
    return dividend % divisor, dividend // divisor

  @staticmethod
  def AssertBases(term1, term2, base_real):
    asserts(term1.Base() == term2.Base(), "ValuesMismatch"\
            , term1.Base(), term2.Base(), locals())
    asserts(term1.Base() == base_real, "ValuesMismatch"\
            , term1.Base(), base_real, locals())

  def ConstructTerms(term1, term2, base):
    self, other = term1, term2
    if not isinstance(term1, TMember):
      self = TMember.FromAny(term1, base)
    if not isinstance(term2, TMember):
      other = TMember.FromAny(term2, base)
    return self, other
  
####################################
####################################
class SAdditionCalc(SCalcBase):
  @staticmethod
  def Add(term1, term2, base):
    #optimize terms and assign augend and addend
    if term1 >= term2:
      augend = TMember.FromAny(term1, base)
      addend = TMember.FromAny(term2, base)
    else:
      augend = TMember.FromAny(term2, base)
      addend = TMember.FromAny(term1, base)    

    #check for bases:
    SCalcBase.AssertBases(augend, addend, base)

    #carry, digit, sum
    c, d = 0, 0
    summand = TMember(augend.Base())

    #start adding
    for i in range(addend.Length()):
      c, d = \
         SAdditionCalc.CarryDigit(augend[i], addend[i]+c, base)
      summand.AppendDigit(d)

    #continue for carry
    for j in range(addend.Length(), augend.Length()):
      c, d = \
         SAdditionCalc.CarryDigit(augend[j], c, base)
      summand.AppendDigit(d)
      
    #copy carry
    if c > 0:
      #carry occured in last digit
      summand.AppendDigit(c)
    return summand;
  
  @staticmethod
  def CarryDigit(d1, d2, base):
    '''returns carry, digit'''
    c, d = 0, 0
    addition_with_carry = d1 + d2
    if addition_with_carry > base:
      c, d = 1, addition_with_carry - base
    elif addition_with_carry == base:
      c, d = 1, 0
    else:
      c, d = 0,  addition_with_carry
    return c, d
  
############################################
class SSubtractionCalc(SCalcBase):
  @staticmethod
  def Absolute(term1, term2, base):
    '''subtraction = minuend - subtrahend'''
    #optimize terms and assign minuend and addend
    minuend, subtrahend = SCalcBase.ConstructTerms(term1, term2, base)    

    #check for bases:
    SCalcBase.AssertBases(minuend, subtrahend, base)

    #carry, digit, difference
    c, d = 0, 0
    difference = TMember(minuend.Base())

    #substract:
    for i in range(subtrahend.Length()):
      c, d = \
         SSubtractionCalc.CarryDigit(minuend[i], subtrahend[i], c, base)
      difference.AppendDigit(d)
    #copy carry
    for j in range(subtrahend.Length(), minuend.Length()):
      c, d = \
         SSubtractionCalc.CarryDigit(minuend[j], 0, c, base)
      difference.AppendDigit(d)
    difference.TrimEnd()
    return difference
    
  @staticmethod
  def CarryDigit(d1, d2, carry, base):
    '''carry is positive and accumulated by d1 - c
        CarryDigit returns carry, digit'''
    c, d = 0, 0
    difference_with_carry = d1 - d2 - carry
    if difference_with_carry >= 0:
      c, d = 0, difference_with_carry
    else:
      c, d = 1, base - abs(difference_with_carry)
    return c, d
  
############################################
class SMultiplicationCalc(SCalcBase):
  @staticmethod
  def Multiply(factor1, factor2, base):
    #generate multiplicand and factor
    if factor1 >= factor2:
      multiplicand = TMember.FromAny(factor1, base)
      multiplier = TMember.FromAny(factor2, base)
    elif factor1 <= factor2:
      multiplicand = TMember.FromAny(factor2, base)
      multiplier = TMember.FromAny(factor1, base)

    #check bases
    SCalcBase.AssertBases(multiplicand, multiplier, base)

    #specify product
    product = TMember.FromInt(0, 10)
    lprd = None
    for i in range(multiplier.Length()):
      lprd = \
           SMultiplicationCalc.MultiplyDigit(multiplicand, multiplier[i], base)
      product += lprd.ShiftLeft(i)
    return product
  
  @staticmethod
  def MultiplyDigit(multiplicand, digit, base):
    if digit == 0 or not multiplicand:
      return TMember.FromInt(0, base)

    #specify product, carry and digit
    prd = TMember(base=10)
    c, d = 0, 0

    #multiply
    for i in range(multiplicand.Length()):
      c, d = \
         SMultiplicationCalc.CarryDigit(multiplicand[i], digit, c, base)
      prd.AppendDigit(d)
    if c > 0:
      prd.AppendDigit(c)
    return prd;

  @staticmethod
  def CarryDigit(d1, d2, carry, base):
    '''returns carry, digit'''
    product_with_carry = d1 * d2 + carry
    c, d = 0, 0
    if product_with_carry > base:
      c, d = product_with_carry // base, product_with_carry % base
    elif product_with_carry == base:
      c, d = 1, 0
    else:
      c, d = 0, product_with_carry
    return c, d
    
##############################################
    
class SDivisionCalc(SCalcBase):
  @staticmethod
  def FloorDivide(dividend, divisor, base):
    '''returns remainder, quotient'''
    #construct right type of terms
    dividend, divisor = SCalcBase.ConstructTerms(dividend, divisor, base)
    if divisor == 0:
      raise ArithmeticError("division by zero!")

    #validate dividend and divisor
    assertf(dividend >= divisor\
            , "dividend<{0}>\nmust be gte then\ndivisor<{1}>"\
            , dividend, divisor)
    
    #assert bases
    SCalcBase.AssertBases(dividend, divisor, base)

    #specify quotient:
    quotient = TMember(base=base)

    #remainder, digit, slice
    r, d = None, None
    #slice from last: 9000 / 12 s= 9
    s = dividend.Slice(-1, -1-divisor.Length())
    if s < divisor:
      # s<2> < 6 then s = 24
      s.AppendDigit(dividend[-1-divisor.Length()], Append.ToLowerMost)
    #divide 24158 / 6
    for i in range(s.Length(), dividend.Length()):
      r, d = SDivisionCalc.DivideSimple(s, divisor, base)
      quotient.AppendDigit(d, Append.ToLowerMost)
      #pull next number from dividend
      s = r.AppendDigit(dividend[-i-1], Append.ToLowerMost)

    r, d = SDivisionCalc.DivideSimple(s, divisor, base)
    quotient.AppendDigit(d, Append.ToLowerMost)
    return r, quotient
      
  @staticmethod
  def DivideSimple(dividend, divisor, base):
    #specify quotient and remainder:
    remainder, quotient = TMember.FromMember(dividend), 0
    while remainder >= divisor:
      remainder -= divisor
      quotient += 1
    return remainder.TrimEnd(), quotient
    
  @staticmethod
  def Modulus(self, mod):
    assertf(isinstance(mod, int)\
           , "only digit mods accepted. mod<{0}> must be typeof<{1}>"\
           , type(mod), type(int))
    assertf(mod < self.Base(), "mod<{0}> must be lt then Base<{1}>"\
           , mod, self.Base())
    modulus, mnext, i = 0, 0, 0
    if self[i] % mod == 0:
      modulus = 0
    else:
      for i in range(self.Length()):
        #append next
        mnext = self[i] % mod
        for j in range(i):
          #mnext may be 3000 = 3.10.10.10
          #3000 % mod = (3%mod)*(10%mod)(10%mod)(10%mod)
          mnext *= self.Base() % mod
          #if there is a zero in between;
          #no need to continue with current i index
          if mnext == 0:
            break
          #correct mnext
          mnext = mnext % mod
        #mnext ok.
        #add it to modulus
        modulus += mnext
        modulus = modulus % mod
    return modulus
      
##############################################
  
class Compliteral:
  pass
Compliteral.Gt = ">"
Compliteral.Lt = "<"
Compliteral.Ge = ">="
Compliteral.Le = "<="
Compliteral.Eq = "=="

class SComparisonCalc(SCalcBase):
  @staticmethod
  def Eq(term1, term2, base):
    self, other = SCalcBase.ConstructTerms(term1, term2, base)
    if self.Length() == other.Length():
      return SComparisonCalc.Compliteralize(self, other, Compliteral.Eq)
    else:
      return False

  @staticmethod
  def Gt(term1, term2, base):
    self, other = SCalcBase.ConstructTerms(term1, term2, base)
    if self.Length() == other.Length():
      return SComparisonCalc.Compliteralize(self, other, Compliteral.Gt)
    else:
      return self.Length() > other.Length()
  @staticmethod      
  def Lt(term1, term2, base):
    self, other = SCalcBase.ConstructTerms(term1, term2, base)
    if self.Length() == other.Length():
      return SComparisonCalc.Compliteralize(self, other, Compliteral.Lt)
    else:
      return self.Length() < other.Length()
    
  @staticmethod
  def Ge(term1, term2, base):
    self, other = SCalcBase.ConstructTerms(term1, term2, base)
    if self.Length() == other.Length():
      return SComparisonCalc.Compliteralize(self, other, Compliteral.Ge)
    else:
      return self > other
    
  @staticmethod
  def Le(term1, term2, base):
    self, other = SCalcBase.ConstructTerms(term1, term2, base)
    if self.Length() == other.Length():
      return SComparisonCalc.Compliteralize(self, other, Compliteral.Le)
    else:
      return self < other
 
  @staticmethod
  def Compliteralize(self, other, compliteral):
    for i in range(self.Length() - 1, -1, -1):
      ret = eval("self[i] {0} other[i]".format(compliteral), locals())
      if self[i] == other[i]:
        continue
      else:
        return ret
    ##came from self[i] == other[i]
    return True

