from uuid import uuid1;

class Arc(object):
  def __init__(self):
    self.Former = None;
    self.Lesser = None;
    self.Guid = uuid1();

  def ArcTo(self, a):
    assert(isinstance(a, Arc));
    a.Lesser = self;
    self.Former = a;

  def Equals(self, a):
    assert(isinstance(a, Arc));
    return self.Guid == a.Guid;
