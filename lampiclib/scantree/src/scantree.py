'''
Created on 31 Oca 2011

@author: dr.ozgur.sonmez
'''
import sys, os

from core.MScanner import Scanner;
sys.path.append("..{0}..{0}walky{0}src".format(os.path.sep))
from walker.MWalker import TWalker


if __name__ == "__main__":
  rootPath = sys.argv[1];
  w = TWalker();
  w.Start();
  s = Scanner(rootPath);
  s.Scan();
  w.StopAndReport();
  print("Element Count = {0}".format(s.Count))
