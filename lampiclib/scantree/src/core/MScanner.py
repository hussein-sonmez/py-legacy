'''
Created on 30 Oca 2011

@author: dr.ozgur.sonmez
'''
import os;
from datalayer.MEntityElement import EntityElement;
from datalayer.MFacadeElement import FacadeElement;

class Scanner(object):
    def __init__(self, rootPath, truncate=True):
        self.RootPath = rootPath;
        self.FacadeElement = FacadeElement();
        if truncate:
          self.FacadeElement.TruncateTable("elements");
        self.Count = 0;
    
    def Scan(self):
        ee = EntityElement()
        root = os.path.split(self.RootPath)[1];
        if not root:
            root = self.RootPath.replace(os.path.sep, "");
        ee.Name = root;
        ee.Directory = True;
        self.FacadeElement.InsertElement(ee);
        self.Count += 1;
        self._ScanRecursive(self.RootPath, ee.ID);
    
    def _ScanRecursive(self, parentPath, parentID):
        elements = [];
        try:
            elements = os.listdir(parentPath);
        except WindowsError as we:
            print(we);
            return;
        files = (f for f in elements if os.path.isfile(os.path.join(parentPath, f)))
        dirs = (d for d in elements if os.path.isdir(os.path.join(parentPath, d)))
        for f in files:
            ee = EntityElement()
            ee.Name = f;
            ee.Directory = False;
            ee.ParentID = parentID;
            self.FacadeElement.InsertElement(ee);
            self.Count += 1;
        for d in dirs:
            ee = EntityElement()
            ee.Name = d;
            ee.Directory = True;
            ee.ParentID = parentID;
            self.FacadeElement.InsertElement(ee);
            self.Count += 1;
            self._ScanRecursive(os.path.join(parentPath, d), ee.ID);
            
   
        
