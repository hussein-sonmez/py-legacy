'''
Created on 31 Oca 2011

@author: dr.ozgur.sonmez
'''
import sys;
try:
  import pymysql;
except ImportError as ie:
  print("pymysql could not be imported..");
  print("exiting..");
  sys.exit(0);

class TFacade(object):
  def __init__(self):
    self.Connection = None;
  
  def Init(self):
    try:
      self.Connection = \
        pymysql.Connection("localhost", "root", "123", "scandb");
      self.Cursor = self.Connection.cursor();
    except Exception as ex:
      print("Connection could not be established\nMessage is: {0}".format(ex));
      return;
    
  def Close(self):
    self.Cursor.close();
    self.Connection.commit();
    self.Connection.close();
  
  def Execute(self, sql):
    if self.Connection:
      self.Cursor.execute(sql);
    else:
      raise Exception();
  
  def Identity(self):
    self.Cursor.execute("SELECT LAST_INSERT_ID();");
    pID = self.Cursor.fetchone()[0];
    return pID;

  def TruncateTable(self, table):
    sql = "TRUNCATE TABLE {0};".format(table);
    self.Init();
    self.Execute(sql);
    self.Close();
      
      
      
