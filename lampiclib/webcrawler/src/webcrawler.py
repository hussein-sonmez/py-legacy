import urllib.request as ur
import sys
import importer;
notifier = importer.importkit("out", "notifiers")

def crawl(rooturl, depth):
  text = ""
  while True:
    notifier.printf("reading url {0}..", rooturl)
    try:
      text = str(ur.urlopen(rooturl).read())
      notifier.printf("{0} read.", rooturl)
      childs = []
    except:
      notifier.printf("url {0} could not be opened.", rooturl)    
  
