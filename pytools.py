class ItemType:
	pass
ItemType.File = 1
ItemType.Directory = 2

import os
import shutil as sh

walker = os.walk
joiner = os.path.join
isfile  = os.path.isfile
isdir  = lambda d: not isfile(d)

class Crawler():
	def __init__(self, root):
		
		self.root = root
	
	def _recurse(self, callback, parent, depth):
		if not isdir(parent): raise Exception('should have been directory. <{0}>'.format(parent)) 
		for root, ds, fs  in walker(parent):
			for f in fs:
				callback(joiner(root, f), ItemType.File, depth)
			for d in ds:
				callback(joiner(root, d), ItemType.Directory, depth)
				self._recurse(callback, joiner(root, d), depth + 1)
			

	def each(self, callback):
		self._recurse(callback, self.root, 0)

			
if __name__ == '__main__':
	def starify(depth):
		#print("{0}".format('-'*depth*2))
		pass
	def cb(path, itemType, depth):
		starify(depth)
		print("<{0}> path is a <{1}>".format(path, itemType == ItemType.File and 'file' or 'directory'))	
	def cb2(path, itemType, depth):
		starify(depth)
		if path.endswith('__init__.py') or path.endswith('setup.py'): 
			os.remove(path)
			print("<{0}> deleted".format(path))	
	def cb3(path, itemType, depth):
		starify(depth)
		if path.endswith('.vspscc') and itemType is ItemType.File: 
			os.remove(path)
			print("<{0}> deleted".format(path))	
	def cb4(path, itemType, depth):
		starify(depth)
		if path.endswith('TestResults') and itemType is ItemType.Directory: 
			sh.rmtree(path)
			print("<{0}> directory removed".format(path))
	def cb5(path, itemType, depth):
		starify(depth)
		if path.endswith('.sln.ide') and itemType is ItemType.Directory: 
			sh.rmtree(path)
			print("<{0}> directory removed".format(path))
	def cb6(path, itemType, depth):
		starify(depth)
		if path.endswith('packages.config') and itemType is ItemType.File:
			os.remove(path)
			print("<{0}> file removed".format(path))	
		if path.endswith('packages') and itemType is ItemType.Directory: 
			sh.rmtree(path)
			print("<{0}> directory removed".format(path))	
	def cb7(path, itemType, depth):
		starify(depth)
		if (path.endswith('obj') or path.endswith('bin')) and itemType is ItemType.Directory: 
			sh.rmtree(path)
			print("<{0}> directory removed".format(path))
	def cb8(path, itemType, depth):
		starify(depth)
		if path.endswith('.pyc') and itemType is ItemType.File: 
			os.remove(path)
			print("<{0}> deleted".format(path))	
	def cb9(path, itemType, depth):
		starify(depth)
		if path.endswith('__init__.py') and itemType is ItemType.File: 
			os.remove(path)
			print("<{0}> deleted".format(path))	
	def cb10(path, itemType, depth):
		starify(depth)
		if path.endswith('__pycache__') and itemType is ItemType.Directory: 
			sh.rmtree(path)
			print("<{0}> deleted".format(path))	
	c = Crawler("P:\Source\Workspaces\Python\lampiclib")

	c.each(cb10)


